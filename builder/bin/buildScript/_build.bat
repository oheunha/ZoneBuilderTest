@echo off

SET RepositoryPath=%1
SET BuildPlatform=%2
SET BuildFilePath=%3
SET BuildConfiguration=%4
SET CountryVersionFileName=%5
SET BranchName=%6
SET BuildVersionDefine=%7
SET BuildSlnFilePath=%8
SET BuildProjectName=%9

SET VersionFileName=Version.h
SET BuildExeFilePath=%BuildFilePath%"%BuildProjectName%.exe"
SET BuildPDBFilePath=%BuildFilePath%"%BuildProjectName%.pdb"
SET BuildMapFilePath=%BuildFilePath%"%BuildProjectName%.map"
SET VersionFilePath=%RepositoryPath%Common\%VersionFileName%
SET GitRevisionFilePath=%RepositoryPath%Common\GitRevision.h
SET CountryVersionFilePath=%RepositoryPath%Common\%CountryVersionFileName%
SET MsBuilPath="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin"


d:
cd %RepositoryPath%

git checkout -- *
git stash
git checkout master
git branch -D %BranchName%
git branch -t %BranchName% origin/%BranchName%
git checkout -f %BranchName%
git reset --hard origin/%BranchName%
git pull origin %BranchName%

powershell if('""' -eq '%BuildVersionDefine%') { return; }; $path = '%VersionFilePath%' -Replace ('"', ''); $path = $path -Replace ('\"', '\'); $path = dir -Path $path -Include '%VersionFileName%' -Recurse; $tmp = Get-Content $path; $tmp = $tmp -Replace ('(^\s*)(//\s*)(#define\s+(%BuildVersionDefine%))', '$1$3'); Set-Content $path $tmp;


c:
cd %MsBuilPath%

MSBuild.exe %BuildSlnFilePath% /t:Clean /p:Configuration=%BuildConfiguration% /p:Platform=%BuildPlatform%
MSBuild.exe %BuildSlnFilePath% /t:Rebuild /p:Configuration=%BuildConfiguration% /p:Platform=%BuildPlatform%

timeout 5 > NUL

IF NOT EXIST %BuildExeFilePath% (
	exit 
)


d:
cd %BuildFilePath%

D:\util\symstore\symstore.exe add /f %BuildProjectName%.exe /s D:\symbols /t %BuildProjectName%
D:\util\symstore\symstore.exe add /f %BuildProjectName%.pdb /s D:\symbols /t %BuildProjectName%
