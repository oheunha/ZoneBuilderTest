﻿using LitJson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoBuilder
{
    public class ServerInfoItem
    {
        public string IpAddr { get; set; }
        public string Port { get; set; }
        public string Id { get; set; }
        public string Password { get; set; }
        public string PatchFilePath { get; set; }
    }

    public class BuildInfoItem
    {
        public string BranchName { get; set; }
        public string CountryVersionFileName { get; set; }
        public List<string> Defines { get; set; }
        public string BuildConfiguration { get; set; }
        public string BuildPlatform { get; set; }
        public string BuildProjectName { get; set; }
        public string BuildProjectSlnName { get; set; }
        public string BuildProjectPath { get; set; }
        public string BuildOutputPath { get; set; }
    }

    class ConfigInfoItem
    {
        public string RepositoryPath { get; set; }

        public Dictionary<string, BuildInfoItem> ZoneBuildInfoList { get; set; }
        public Dictionary<string, BuildInfoItem> AccountBuildInfoList { get; set; }
        public Dictionary<string, BuildInfoItem> CharacterBuildInfoList { get; set; }
        public Dictionary<string, BuildInfoItem> InterBuildInfoList { get; set; }
    }

    class ConfigInfo
    {
        private const string CONFIG_FILE_NAME = @".\config\configure.json";

        private ConfigInfoItem _configInfo;

        public ConfigInfoItem Info { get {return _configInfo;} }

        public void SetupConfig(Form form)
        {
            try
            {
                using (var file = new StreamReader(CONFIG_FILE_NAME))
                {
                    string jsonData = "";
                    string line = "";
                    while ((line = file.ReadLine()) != null)
                    {
                        jsonData += line;
                    }

                    _configInfo = JsonMapper.ToObject<ConfigInfoItem>(jsonData);
                }
            }
            catch (Exception e)
            {
                MessageBoxHelper.Show(form, e.Message, "error");
                return;
            }
        }
    }
}
