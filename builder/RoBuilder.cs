﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Timers;

namespace RoBuilder
{
    public partial class RoBuilder : Form
    {
        private const string RUN_BUILD_BAT_FILE_NAME = @"run_build.bat";
        private const string TMP_FOLDER_PATH = @"\tmp";
        private const string SCRIPT_FOLDER_PATH = @"\buildScript";

        private ConfigInfo _configInfo = null;
        private string _currentBuildInfoType = string.Empty;

        public string TMP_Path { get { return Directory.GetCurrentDirectory() + TMP_FOLDER_PATH + "\\"; } }
        public string Script_Path { get { return Directory.GetCurrentDirectory() + SCRIPT_FOLDER_PATH + "\\"; } }
        private string BuildBatTmpFilePath { get { return TMP_Path + RUN_BUILD_BAT_FILE_NAME; } }
        private string CommonFilePath { get { return StringHelper.ToPath(_configInfo.Info.RepositoryPath) + "common\\"; } }

        private List<BuildInfoItem>     _reservedBuildList = new List<BuildInfoItem>();
        private System.Timers.Timer     _timer = new System.Timers.Timer(1000);
        private int                     _currentBuildIndex = 0;
        
        public RoBuilder()
        {
            InitializeComponent();
            InitializeConfig();
            SetupBuildTypeInfoUI();

            _timer.Elapsed += TimerEvent;
            _timer.Enabled = true;

            if (false == Directory.Exists(TMP_Path))
            {
                Directory.CreateDirectory(TMP_Path);
            }
        }

        private void InitializeConfig()
        {
            _configInfo = new ConfigInfo();
            _configInfo.SetupConfig(this);
        }

        private void SetupBuildTypeInfoUI()
        {
            foreach (var info in typeof(ConfigInfoItem).GetMethods())
            {
                if (MemberTypes.Method == info.MemberType)
                {
                    Match mBuildListNamePattern = Regex.Match(info.Name, @"(get_)(\w+)(BuildInfoList)");
                    if (true == mBuildListNamePattern.Success)
                    {
                        this.ServerTypeComboBox.Items.Add(mBuildListNamePattern.Groups[2].Value);
                    }
                }
            }
        }

        private void TimerEvent(Object source, ElapsedEventArgs e)
        {
            _timer.Enabled = false;
            this.ProcessBuild();
            _timer.Enabled = true;
        }
        
        private void ProcessBuild()
        {
            if (0 == _reservedBuildList.Count)
            {
                return;
            }

            if (0 == this.BuildList.Items.Count)
            {
                MessageBoxHelper.Show(this, "not exist build name in buildList", "fatal error");
                return;
            }

            if(this.BuildList.Items.Count <= _currentBuildIndex)
            {
                return;
            }
            
            // prepare build
            {
                {
                    string buildName = this.BuildList.Items[_currentBuildIndex].ToString();
                    string patten = @"(\(\w+\))(\s*\w+\s*_\s*\w+)";
                    Regex regex = new Regex(patten);
                    buildName = regex.Replace(buildName, "(build)$2");
                    ControlHelper.UpdateListBoxItem(this, this.BuildList, _currentBuildIndex, buildName);
                }
            }

            // build
            string resultOutputPath = string.Empty;
            bool buildResult = DoBuild(_reservedBuildList[_currentBuildIndex], ref resultOutputPath);
            
            // complete build
            {
                string resultString = (true == buildResult) ? "complete" : "failed";

                {
                    string buildName = this.BuildList.Items[_currentBuildIndex].ToString();
                    string patten = @"(\(\w+\))(\s*\w+\s*_\s*\w+)";
                    Regex regex = new Regex(patten);
                    buildName = regex.Replace(buildName, string.Format("({0})$2 [output path : {1}]", resultString, resultOutputPath));
                    ControlHelper.UpdateListBoxItem(this, this.BuildList, _currentBuildIndex, buildName);
                }
            }

            ++_currentBuildIndex;
        }

        private bool DoBuild(BuildInfoItem buildInfo, ref string resultOutputPath)
        {
            string buildVersionDefine = string.Empty;
            foreach (var defineName in buildInfo.Defines)
            {
                if (false == string.IsNullOrEmpty(buildVersionDefine))
                {
                    buildVersionDefine += "|";
                }
                buildVersionDefine += defineName;
            }

            string buildFilePath = StringHelper.ToPath(_configInfo.Info.RepositoryPath)
                                    + StringHelper.ToPath(buildInfo.BuildProjectPath)
                                    + StringHelper.ToPath(buildInfo.BuildConfiguration);

            string buildProjectSlnPath = StringHelper.ToPath(_configInfo.Info.RepositoryPath)
                                            + StringHelper.ToPath(buildInfo.BuildProjectPath)
                                            + buildInfo.BuildProjectSlnName;

            string repositoryPath = StringHelper.ToPath(_configInfo.Info.RepositoryPath);

            if (true == Directory.Exists(buildFilePath))
            {
                try
                {
                    Directory.Delete(buildFilePath, true);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            using (var file = new StreamWriter(this.BuildBatTmpFilePath, false))
            {
                file.WriteLine("@echo off");
                
                file.WriteLine(string.Format("SET RepositoryPath=\"{0}\"", repositoryPath));
                file.WriteLine(string.Format("SET BuildPlatform={0}", buildInfo.BuildPlatform));

                file.WriteLine(string.Format("SET BuildConfiguration=\"{0}\"", buildInfo.BuildConfiguration));
                file.WriteLine(string.Format("SET BranchName={0}", buildInfo.BranchName));
                file.WriteLine(string.Format("SET CountryVersionFileName={0}", buildInfo.CountryVersionFileName));
                file.WriteLine(string.Format("SET BuildVersionDefine=\"{0}\"", buildVersionDefine));
                file.WriteLine(string.Format("SET BuildProjectName={0}", buildInfo.BuildProjectName));

                file.WriteLine(string.Format("SET BuildFilePath=\"{0}\"", buildFilePath));
                file.WriteLine(string.Format("SET BuildProjectSlnPath=\"{0}\"", buildProjectSlnPath));

                file.WriteLine(StringHelper.ToDrivePath(this.Script_Path));
                file.WriteLine(string.Format("cd \"{0}\"", this.Script_Path));
                file.WriteLine("_build.bat %RepositoryPath% %BuildPlatform% %BuildFilePath% %BuildConfiguration% %CountryVersionFileName% %BranchName% %BuildVersionDefine% %BuildProjectSlnPath% %BuildProjectName%");
            }
            
            {
                Process p = new Process();
                p.StartInfo.WorkingDirectory = TMP_Path;
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = "/c " + RUN_BUILD_BAT_FILE_NAME;
                p.Start();
                p.WaitForExit();
            }

            {
                if (false == File.Exists(buildFilePath + buildInfo.BuildProjectName + ".exe"))
                {
                    return false;
                }
            }

            {
                string outputPath = StringHelper.ToPath(buildInfo.BuildOutputPath)
                                    + StringHelper.ToPath(DateTime.Now.ToString("yyyyMMdd_HHmmss")
                                                          + "_" + buildInfo.BranchName + "_" + buildInfo.BuildProjectName);

                if (false == Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }
                
				try
                {
	                File.Copy(buildFilePath + buildInfo.BuildProjectName + ".exe", outputPath + buildInfo.BuildProjectName + ".exe");
	                File.Copy(buildFilePath + buildInfo.BuildProjectName + ".pdb", outputPath + buildInfo.BuildProjectName + ".pdb");
	                File.Copy(buildFilePath + buildInfo.BuildProjectName + ".map", outputPath + buildInfo.BuildProjectName + ".map");
	                File.Copy(CommonFilePath + buildInfo.CountryVersionFileName, outputPath + buildInfo.CountryVersionFileName);
	                File.Copy(CommonFilePath + "Version.h", outputPath + "Version.h");
	                File.Copy(CommonFilePath + "GitRevision.h", outputPath + "GitRevision.h");
				}
				catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                resultOutputPath = outputPath;
            }
            
            return true;
        }

        private void AddToBuildListButton_Click(object sender, EventArgs e)
        {
            if (this.ServerTypeComboBox.SelectedIndex < 0)
            {
                MessageBoxHelper.Show(this, "Select server type first", "error");
                return;
            }

            if (this.BuildTypeComboBox.SelectedIndex < 0)
            {
                MessageBoxHelper.Show(this, "Select build type first", "error");
                return;
            }

            string serverType = this.ServerTypeComboBox.Items[this.ServerTypeComboBox.SelectedIndex].ToString();
            var serverBuildInfoList = GetBuildInfoList(serverType);
            if (null == serverBuildInfoList)
            {
                MessageBoxHelper.Show(this, string.Format("not exist server type {0}", serverType), "error");
                return;
            }

            string buildType = this.BuildTypeComboBox.Items[this.BuildTypeComboBox.SelectedIndex].ToString();
            if (false == serverBuildInfoList.ContainsKey(buildType))
            {
                MessageBoxHelper.Show(this, string.Format("not exist build type {0}", buildType), "error");
                return;
            }

            this.BuildList.Items.Add("(reserved)" + buildType + "_" + serverType);
            _reservedBuildList.Add(serverBuildInfoList[buildType]);
        }

        private Dictionary<string, BuildInfoItem> GetBuildInfoList(string serverType)
        {
            if (true == string.IsNullOrEmpty(serverType))
            {
                return null;
            }

            object obj = typeof(ConfigInfoItem).GetMethod("get_" + serverType + "BuildInfoList").Invoke(_configInfo.Info, null);
            if (null == obj)
            {
                return null;
            }

            return (Dictionary<string, BuildInfoItem>)obj;
        }

        private void ServerTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ServerTypeComboBox.SelectedIndex < 0)
            {
                MessageBoxHelper.Show(this, "Select server type first", "error");
                return;
            }

            string serverType = this.ServerTypeComboBox.Items[this.ServerTypeComboBox.SelectedIndex].ToString();
            var serverBuildInfoList = GetBuildInfoList(serverType);
            if (null == serverBuildInfoList)
            {
                MessageBoxHelper.Show(this, string.Format("not exist server type (%0)", serverType), "error");
                return;
            }

            this.BuildTypeComboBox.Items.Clear();

            foreach (var item in serverBuildInfoList)
            {
                this.BuildTypeComboBox.Items.Add(item.Key);
            }
        }

        private void BuildTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ServerTypeComboBox.SelectedIndex < 0)
            {
                MessageBoxHelper.Show(this, "Select server type first", "error");
                return;
            }

            if (this.BuildTypeComboBox.SelectedIndex < 0)
            {
                return;
            }
        }

        private void PriorityUpButton_Click(object sender, EventArgs e)
        {

        }

        private void PriorityDownButton_Click(object sender, EventArgs e)
        {

        }

        private void RemoveSelectedItemButton_Click(object sender, EventArgs e)
        {
            MessageBoxHelper.Show(this, "Reserved build item only", "notify");
        }











        //////////////////////////////////////////////////////////////////////////////////////////////////
        /// not used
        ////////////////////////////////////////////////////////////////////////////////////////////////// 
        #region NotUsed
        private BuildInfoItem GetBuildInfo()
        {
            //object selectedItem = this.ServerTypeComboBox.SelectedItem;

            //if (null != selectedItem)
            //{
            //    _currentBuildInfoType = selectedItem.ToString();
            //    if (false == _configInfo.Info.BuildInfoList.ContainsKey(_currentBuildInfoType))
            //    {
            //        _currentBuildInfoType = string.Empty;
            //        MessageBoxHelper.Show(this, "Initializing build info failed", "error");
            //        return null;
            //    }

            //    return _configInfo.Info.BuildInfoList[_currentBuildInfoType];
            //}

            return null;
        }

        private void RefreshDefData()
        {
            //_parser.Parse(this, this.CountyVersionFileNameTextBox.Text);
            //SetupDefDataUI(_parser);
        }
        
        private void InitializeBranch()
        {
            //string currentBranchName = string.Empty;

            //{
            //    Process p = new Process();
            //    p.StartInfo.WorkingDirectory = this.RepositoryPathTextBox.Text;
            //    p.StartInfo.FileName = "cmd.exe";
            //    p.StartInfo.Arguments = "/c git branch";
            //    p.StartInfo.UseShellExecute = false;
            //    p.StartInfo.RedirectStandardOutput = true;
            //    p.StartInfo.RedirectStandardError = true;
            //    p.Start();
            //    string output = p.StandardOutput.ReadToEnd();
            //    p.WaitForExit();
                
            //    Match mBranchNamePattern = Regex.Match(output, @"(\*\s*)(\w+)(\s*)");
            //    if (true == mBranchNamePattern.Success)
            //    {
            //        currentBranchName = mBranchNamePattern.Groups[2].Value;
            //    }
            //}

            //if (0 != string.Compare(currentBranchName, this.BuildBranchNameTextBox.Text))
            //{
            //    Process p = new Process();
            //    p.StartInfo.WorkingDirectory = this.RepositoryPathTextBox.Text;
            //    p.StartInfo.FileName = "cmd.exe";
            //    p.StartInfo.Arguments = string.Format("/c git stash save \"save_from_build_tool\" & git checkout {0} & git pull & git reset --hard origin/{1}", this.BuildBranchNameTextBox.Text, this.BuildBranchNameTextBox.Text);
            //    p.Start();
            //    p.WaitForExit();
            //}
        }

        private void EnableUI(bool enable)
        {
            // build type ui
            ControlHelper.EnableComboBox(this, this.ServerTypeComboBox, enable);

            ControlHelper.EnableButton(this, this.AddToBuildListButton, enable);
            ControlHelper.EnableButton(this, this.PriorityUpButton, enable);
            ControlHelper.EnableButton(this, this.PriorityDownButton, enable);
            ControlHelper.EnableButton(this, this.RemoveSelectedItemButton, enable);
        }
        
       
        private void LocalListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listbox = (ListBox)sender;
            if (listbox.SelectedIndex < 0)
            {
                return;
            }

            //this.VersionSelectedDefTextBox.Text = listbox.Items[listbox.SelectedIndex].ToString();
        }

        private void SakrayListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listbox = (ListBox)sender;
            if (listbox.SelectedIndex < 0)
            {
                return;
            }

            //this.VersionSelectedDefTextBox.Text = listbox.Items[listbox.SelectedIndex].ToString();
        }

        private void MainListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListBox listbox = (ListBox)sender;
            if (listbox.SelectedIndex < 0)
            {
                return;
            }

            //this.VersionSelectedDefTextBox.Text = listbox.Items[listbox.SelectedIndex].ToString();
        }
        
        private void LocalToSakrayButton_Click(object sender, EventArgs e)
        {
            //if (this.VersionLocalListBox.SelectedIndex < 0)
            //{
            //    MessageBoxHelper.Show(this, "LocalListBox.SelectedIndex -> " + this.VersionLocalListBox.SelectedIndex, "error");
            //    return;
            //}

            //string defKey = this.VersionLocalListBox.Items[this.VersionLocalListBox.SelectedIndex].ToString();

            //_parser.MoveDefListItemFromLocalToSakray(defKey);
            //SetupDefDataUI(_parser);
        }

        private void SakrayToLocalButton_Click(object sender, EventArgs e)
        {
            //if (this.VersionSakrayListBox.SelectedIndex < 0)
            //{
            //    MessageBoxHelper.Show(this, "SakrayListBox.SelectedIndex -> " + this.VersionSakrayListBox.SelectedIndex, "error");
            //    return;
            //}

            //string defKey = this.VersionSakrayListBox.Items[this.VersionSakrayListBox.SelectedIndex].ToString();

            //_parser.MoveDefListItemFromSakrayToLocal(defKey);
            //SetupDefDataUI(_parser);
        }

        private void SakrayToMainButton_Click(object sender, EventArgs e)
        {
            //if (this.VersionMaintenanceNameComboBox.SelectedIndex < 0)
            //{
            //    MessageBoxHelper.Show(this, "MaintenanceNameComboBox.SelectedIndex -> " + this.VersionMaintenanceNameComboBox.SelectedIndex, "error");
            //    return;
            //}

            //string maintenanceKey = this.VersionMaintenanceNameComboBox.Items[VersionMaintenanceNameComboBox.SelectedIndex].ToString();
            //if (false == _parser.MainDefList.ContainsKey(maintenanceKey))
            //{
            //    MessageBoxHelper.Show(this, string.Format("maintenanceKey is not exist (key: {0})", maintenanceKey), "error");
            //    return;
            //}

            //if (this.VersionSakrayListBox.SelectedIndex < 0)
            //{
            //    MessageBoxHelper.Show(this, "SakrayListBox.SelectedIndex -> " + this.VersionSakrayListBox.SelectedIndex, "error");
            //    return;
            //}

            //string defKey = this.VersionSakrayListBox.Items[this.VersionSakrayListBox.SelectedIndex].ToString();

            //_parser.MoveDefListItemFromSakrayToMain(maintenanceKey, defKey);
            //SetupDefDataUI(_parser);
        }

        private void MainToSakrayButton_Click(object sender, EventArgs e)
        {
            //if (this.VersionMaintenanceNameComboBox.SelectedIndex < 0)
            //{
            //    MessageBoxHelper.Show(this, "MaintenanceNameComboBox.SelectedIndex -> " + this.VersionMaintenanceNameComboBox.SelectedIndex, "error");
            //    return;
            //}

            //string maintenanceKey = this.VersionMaintenanceNameComboBox.Items[VersionMaintenanceNameComboBox.SelectedIndex].ToString();
            //if (false == _parser.MainDefList.ContainsKey(maintenanceKey))
            //{
            //    MessageBoxHelper.Show(this, string.Format("maintenanceKey is not exist (key: {0})", maintenanceKey), "error");
            //    return;
            //}

            //if (this.VersionMainListBox.SelectedIndex < 0)
            //{
            //    MessageBoxHelper.Show(this, "MainListBox.SelectedIndex -> " + this.VersionMainListBox.SelectedIndex, "error");
            //    return;
            //}

            //string defKey = this.VersionMainListBox.Items[this.VersionMainListBox.SelectedIndex].ToString();

            //_parser.MoveDefListItemFromMainToSakray(maintenanceKey, defKey);
            //SetupDefDataUI(_parser);
        }

        private void VersionRefreshButton_Click(object sender, EventArgs e)
        {
            //if (string.IsNullOrEmpty(this.RepositoryPathTextBox.Text))
            //{
            //    MessageBoxHelper.Show(this, "RepositoryPath is empty", "error");
            //    return;
            //}
            
            //{
            //    Process p = new Process();
            //    p.StartInfo.WorkingDirectory = this.RepositoryPathTextBox.Text;
            //    p.StartInfo.FileName = "cmd.exe";
            //    p.StartInfo.Arguments = string.Format("/c git reset --hard origin/{0} & git pull", this.BuildBranchNameTextBox.Text);
            //    p.Start(); 
            //    p.WaitForExit();
            //}

            //RefreshDefData();
        }
        
        private void ShowDiffWindow()
        {
            //if (string.IsNullOrEmpty(this.CountyVersionFileNameTextBox.Text))
            //{
            //    MessageBoxHelper.Show(this, "CountryVersionFilePath is empty", "error");
            //    return;
            //}
            
            //Process p = new Process();
            //p.StartInfo.FileName = "TortoiseGitMerge.exe";
            //p.StartInfo.Arguments = "\"" + this.CountyVersionFileNameTextBox.Text + "\" \"" + this.SaveTmpFilePath + "\"";
            //p.Start();
            //p.WaitForExit();

            //string versionFileName = String.Empty;
            //Match mVersionFileNamePattern = Regex.Match(this.CountyVersionFileNameTextBox.Text, @"(.+\\)(\w+\.h)");
            //if (true == mVersionFileNamePattern.Success)
            //{
            //    versionFileName = mVersionFileNamePattern.Groups[2].Value;
            //}
            
            //DialogResult result = MessageBoxHelper.Show(this, "Do you want to apply modified info to " + versionFileName + "?", "notify", MessageBoxButtons.YesNo);
            //if (DialogResult.Yes == result)
            //{
            //    File.Delete(this.CountyVersionFileNameTextBox.Text);
            //    File.Copy(this.SaveTmpFilePath, this.CountyVersionFileNameTextBox.Text);
            //    MessageBoxHelper.Show(this, "Save file complete", "notify");
            //}
        }

        private void VersionSaveButton_Click(object sender, EventArgs e)
        {
            //EnableUI(false);

            //_parser.SerializeVersionFile(this.CountyVersionFileNameTextBox.Text, this.SaveTmpFilePath);

            //ShowDiffWindow();
            //EnableUI(true);
        }

        private void CommitButton_Click(object sender, EventArgs e)
        {
            //EnableUI(false);

            //DialogResult result = MessageBoxHelper.Show(this, "Do you want to run commit and push process?", "confirm", MessageBoxButtons.YesNo);
            //if (DialogResult.Yes == result)
            //{
            //    // todo : master 에 커밋하고, 변경된 내용의 커밋로그를 가져와서 branchName 의 브랜치에 체리픽&push 까지 진행?
            //    Process p = new Process();
            //    p.StartInfo.WorkingDirectory = this.RepositoryPathTextBox.Text;
            //    p.StartInfo.FileName = "cmd.exe";
            //    p.StartInfo.Arguments = "/c git add . & git commit -m \"디파인 수정\" & git push"; 
            //    p.Start();
            //    p.WaitForExit();

            //    MessageBoxHelper.Show(this, "Commit & Push process is completed", "notify");
            //}

            //EnableUI(true);
        }
        
        private void ProcessBuildxxxx()
        {
            //if ((this.ServerTypeComboBox.SelectedIndex < 0))
            //{
            //    MessageBoxHelper.Show(this, "Select server type first", "error");
            //    return;
            //}

            //if ((this.BuildTypeComboBox.SelectedIndex < 0))
            //{
            //    MessageBoxHelper.Show(this, "Select build type first", "error");
            //    return;
            //}

            //string serverType = this.ServerTypeComboBox.Items[this.ServerTypeComboBox.SelectedIndex].ToString();
            //var serverBuildInfoList = GetBuildInfoList(serverType);
            //if (null == serverBuildInfoList)
            //{
            //    MessageBoxHelper.Show(this, string.Format("not exist server type {0}", serverType), "error");
            //    return;
            //}

            //string buildType = this.BuildTypeComboBox.Items[this.BuildTypeComboBox.SelectedIndex].ToString();
            //if (false == serverBuildInfoList.ContainsKey(buildType))
            //{
            //    MessageBoxHelper.Show(this, string.Format("not exist build type {0}", buildType), "error");
            //    return;
            //}

            //BuildInfoItem buildInfo = serverBuildInfoList[buildType];
            //if(null == buildInfo)
            //{
            //    MessageBoxHelper.Show(this, string.Format("not exist build info item {0}", buildType), "error");
            //    return;
            //}

            //DialogResult result = MessageBoxHelper.Show(this, "Do you want to run build process?", "confirm", MessageBoxButtons.YesNo);
            //if (DialogResult.Yes == result)
            //{
            //    EnableUI(false);

            //    if (false == string.IsNullOrEmpty(this.BuildBatTmpFilePath))
            //    {
            //        if (false == Directory.Exists(StringHelper.ToPath(this.BuildOutputPathTextBox.Text)))
            //        {
            //            Directory.CreateDirectory(StringHelper.ToPath(this.BuildOutputPathTextBox.Text));
            //        }

            //        List<string> copyFileList = new List<string>();
            //        copyFileList.Add(this.ZoneProcessSlnPath + StringHelper.ToPath(this.BuildConfigTextBox.Text) + buildInfo.BuildResultName + ".exe");
            //        copyFileList.Add(this.ZoneProcessSlnPath + StringHelper.ToPath(this.BuildConfigTextBox.Text) + buildInfo.BuildResultName + ".pdb");
            //        copyFileList.Add(this.ZoneProcessSlnPath + StringHelper.ToPath(this.BuildConfigTextBox.Text) + buildInfo.BuildResultName + ".map");
            //        copyFileList.Add(this.CountyVersionFileNameTextBox.Text);
            //        //copyFileList.Add("GitRevision.h");
            //        copyFileList.Add(this.VersionFileName);

            //        {
            //            foreach (var filePath in copyFileList)
            //            {
            //                Match mFileName = Regex.Match(filePath, @"[\w\.]+$");
            //                if (true == mFileName.Success)
            //                {
            //                    File.Delete(StringHelper.ToPath(this.BuildOutputPathTextBox.Text) + mFileName.Value);
            //                }
            //            }
            //        }

            //        using (var file = new StreamWriter(this.BuildBatTmpFilePath, false))
            //        {
            //            file.WriteLine("@echo off");
            //            file.WriteLine(StringHelper.ToDrivePath(this.RepositoryPathTextBox.Text));
            //            file.WriteLine(string.Format("cd \"{0}\"", StringHelper.ToPath(this.RepositoryPathTextBox.Text)));

            //            Match mDefinesPattern = Regex.Match(this.BuildDefinesTextBox.Text, @"(\w+)");
            //            while (true == mDefinesPattern.Success)
            //            {
            //                file.WriteLine("powershell \"dir -Path '" + StringHelper.ToPath(this.VersionFilePath) + "' -Include 'version.h' -Recurse | %%{$tmp = Get-Content $_; $tmp=$tmp -Replace('(^\\s*)(//\\s*)(#define\\s+" + mDefinesPattern.Value + ")', '$1$3'); Set-Content $_ $tmp}\"");
            //                mDefinesPattern = mDefinesPattern.NextMatch();
            //            }

            //            file.WriteLine(StringHelper.ToDrivePath(_configInfo.Info.MsBuildPath));
            //            file.WriteLine(string.Format("cd \"{0}\"", _configInfo.Info.MsBuildPath));

            //            file.WriteLine(string.Format("MSBuild.exe \"{0}\" /t:Rebuild /p:Configuration=\"{1}\" /p:Platform=\"{2}\"",
            //                                         this.ZoneProcessSlnPath + PROJECT_NAME + ".sln",
            //                                         this.BuildConfigTextBox.Text,
            //                                         this.BuildPlatformTextBox.Text
            //                                         ));

            //            file.WriteLine("timeout 5 > NUL");

            //            foreach (var filePath in copyFileList)
            //            {
            //                file.WriteLine(string.Format("COPY \"{0}\" \"{1}\"",
            //                                             filePath,
            //                                             StringHelper.ToPath(this.BuildOutputPathTextBox.Text)));
            //            }
            //        }

            //        {
            //            Process p = new Process();
            //            p.StartInfo.WorkingDirectory = TMP_Path;
            //            p.StartInfo.FileName = "cmd.exe";
            //            p.StartInfo.Arguments = "/c build.bat";
            //            p.Start();
            //            p.WaitForExit();
            //        }

            //        {
            //            foreach (var filePath in copyFileList)
            //            {
            //                Match mFileName = Regex.Match(filePath, @"[\w\.]+$");
            //                if (true == mFileName.Success)
            //                {
            //                    if (false == File.Exists(StringHelper.ToPath(this.BuildOutputPathTextBox.Text) + mFileName.Value))
            //                    {
            //                        MessageBoxHelper.Show(this, "Build Failed", "error");
            //                        EnableUI(true);
            //                        return;
            //                    }
            //                }
            //            }
            //        }

            //        {
            //            //if (false == string.IsNullOrEmpty(this.BuildTagTextBox.Text))
            //            //{
            //            //    Process p = new Process();
            //            //    p.StartInfo.WorkingDirectory = StringHelper.ToPath(this.RepositoryPathTextBox.Text);
            //            //    p.StartInfo.FileName = "cmd.exe";
            //            //    p.StartInfo.Arguments = string.Format("/c git tag {0} & git push --tag", this.BuildTagTextBox.Text);
            //            //    p.Start();
            //            //    p.WaitForExit();
            //            //}
            //        }
            //    }

            //    MessageBoxHelper.Show(this, "Build process is completed", "notify");
            //    EnableUI(true);
            //}
        }

        private void LogProcess_Exited(RoBuilder form, string revisionFileName)
        {
            //var timeout = DateTime.Now.Add(TimeSpan.FromSeconds(3));
            //while (!File.Exists(RevisionTmpFilePath))
            //{
            //    if (DateTime.Now > timeout)
            //    {
            //        EnableUI(true);
            //        return;
            //    }
            //}

            //using (var file = new StreamReader(RevisionTmpFilePath))
            //{
            //    string line = file.ReadLine();
            //    if (false == string.IsNullOrEmpty(line))
            //    {
            //        //ControlHelper.SetTextBox(form, form.BuildRevisionTextBox, line);
            //    }
            //}

            //EnableUI(true);
        }

        private void RevisionSearchButton_Click(object sender, EventArgs e)
        {
            //if (File.Exists(TMP_Path + "\\revision.tmp"))
            //{
            //    File.Delete(TMP_Path + "\\revision.tmp");
            //}
            
            //ProcessStartInfo procInfo = new ProcessStartInfo();
            //procInfo.FileName = @"TortoiseGitProc.exe";
            //procInfo.Arguments = string.Format("/command:log /path:\"{0}\" /outfile:\"{1}\"", StringHelper.ToPath(this.RepositoryPathTextBox.Text), RevisionTmpFilePath);
            //procInfo.UseShellExecute = false;
            //procInfo.RedirectStandardOutput = true;
            //procInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //procInfo.CreateNoWindow = true;

            //Process process = Process.Start(procInfo);
            //process.EnableRaisingEvents = true;
            //process.Exited += (log_sender, log_e) => LogProcess_Exited(this, RevisionTmpFilePath);

            //EnableUI(false);
        }
  
        private void PatchIndoorButton_Click(object sender, EventArgs e)
        {
            
        }

        private void BuildTypeApplyButton_Click(object sender, EventArgs e)
        {
        }

        private void PatchAllButton_Click(object sender, EventArgs e)
        {
        }

        private void BuildTypeSaveButton_Click(object sender, EventArgs e)
        {

        }

        private void ZoneBuilder_Load(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }
        #endregion // NotUsed
    }
}
