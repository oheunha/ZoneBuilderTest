﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace RoBuilder
{
    class StringHelper
    {
        static public string ToPath(string pathString)
        {
            pathString = pathString.Trim();

            Match mPath = Regex.Match(pathString, @"^(\.\\)(.+)");
            if (true == mPath.Success)
            {
                pathString = Directory.GetCurrentDirectory() + "\\" + mPath.Groups[2];
            }

            mPath = Regex.Match(pathString, @"(.+\w+)(\s*$)");
            if (true == mPath.Success)
            {
                return mPath.Groups[1].Value + "\\"; 
            }

            return pathString;
        }

        static public string ToDrivePath(string pathString)
        {
            pathString = pathString.Trim();

            Match mPath = Regex.Match(pathString, @"^\.\\");
            if (true == mPath.Success)
            {
                pathString = Directory.GetCurrentDirectory();
            }
            
            mPath = Regex.Match(pathString, @"^\w:");
            if (true == mPath.Success)
            {
                return mPath.Value;
            }

            return string.Empty;
        }
    }
}
