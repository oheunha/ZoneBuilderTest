﻿namespace RoBuilder
{
    partial class RoBuilder
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.RemoveSelectedItemButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.ServerTypeComboBox = new System.Windows.Forms.ComboBox();
            this.BuildTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.BuildList = new System.Windows.Forms.ListBox();
            this.AddToBuildListButton = new System.Windows.Forms.Button();
            this.PriorityUpButton = new System.Windows.Forms.Button();
            this.PriorityDownButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RemoveSelectedItemButton
            // 
            this.RemoveSelectedItemButton.Location = new System.Drawing.Point(317, 475);
            this.RemoveSelectedItemButton.Name = "RemoveSelectedItemButton";
            this.RemoveSelectedItemButton.Size = new System.Drawing.Size(131, 35);
            this.RemoveSelectedItemButton.TabIndex = 18;
            this.RemoveSelectedItemButton.Text = "선택 삭제";
            this.RemoveSelectedItemButton.UseVisualStyleBackColor = true;
            this.RemoveSelectedItemButton.Click += new System.EventHandler(this.RemoveSelectedItemButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 12);
            this.label8.TabIndex = 23;
            this.label8.Text = "Server Type :";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ServerTypeComboBox
            // 
            this.ServerTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ServerTypeComboBox.FormattingEnabled = true;
            this.ServerTypeComboBox.Location = new System.Drawing.Point(99, 15);
            this.ServerTypeComboBox.Name = "ServerTypeComboBox";
            this.ServerTypeComboBox.Size = new System.Drawing.Size(201, 20);
            this.ServerTypeComboBox.TabIndex = 18;
            this.ServerTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ServerTypeComboBox_SelectedIndexChanged);
            // 
            // BuildTypeComboBox
            // 
            this.BuildTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BuildTypeComboBox.FormattingEnabled = true;
            this.BuildTypeComboBox.Location = new System.Drawing.Point(98, 51);
            this.BuildTypeComboBox.Name = "BuildTypeComboBox";
            this.BuildTypeComboBox.Size = new System.Drawing.Size(202, 20);
            this.BuildTypeComboBox.TabIndex = 26;
            this.BuildTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.BuildTypeComboBox_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 12);
            this.label14.TabIndex = 27;
            this.label14.Text = "Build Type :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // BuildList
            // 
            this.BuildList.FormattingEnabled = true;
            this.BuildList.HorizontalScrollbar = true;
            this.BuildList.ItemHeight = 12;
            this.BuildList.Location = new System.Drawing.Point(14, 87);
            this.BuildList.Name = "BuildList";
            this.BuildList.ScrollAlwaysVisible = true;
            this.BuildList.Size = new System.Drawing.Size(434, 376);
            this.BuildList.TabIndex = 18;
            // 
            // AddToBuildListButton
            // 
            this.AddToBuildListButton.Location = new System.Drawing.Point(317, 12);
            this.AddToBuildListButton.Name = "AddToBuildListButton";
            this.AddToBuildListButton.Size = new System.Drawing.Size(131, 59);
            this.AddToBuildListButton.TabIndex = 28;
            this.AddToBuildListButton.Text = "빌드 추가";
            this.AddToBuildListButton.UseVisualStyleBackColor = true;
            this.AddToBuildListButton.Click += new System.EventHandler(this.AddToBuildListButton_Click);
            // 
            // PriorityUpButton
            // 
            this.PriorityUpButton.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.PriorityUpButton.Location = new System.Drawing.Point(17, 475);
            this.PriorityUpButton.Name = "PriorityUpButton";
            this.PriorityUpButton.Size = new System.Drawing.Size(65, 35);
            this.PriorityUpButton.TabIndex = 31;
            this.PriorityUpButton.Text = "+";
            this.PriorityUpButton.UseVisualStyleBackColor = true;
            this.PriorityUpButton.Click += new System.EventHandler(this.PriorityUpButton_Click);
            // 
            // PriorityDownButton
            // 
            this.PriorityDownButton.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.PriorityDownButton.Location = new System.Drawing.Point(88, 475);
            this.PriorityDownButton.Name = "PriorityDownButton";
            this.PriorityDownButton.Size = new System.Drawing.Size(65, 35);
            this.PriorityDownButton.TabIndex = 32;
            this.PriorityDownButton.Text = "-";
            this.PriorityDownButton.UseVisualStyleBackColor = true;
            this.PriorityDownButton.Click += new System.EventHandler(this.PriorityDownButton_Click);
            // 
            // RoBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 522);
            this.Controls.Add(this.PriorityDownButton);
            this.Controls.Add(this.PriorityUpButton);
            this.Controls.Add(this.AddToBuildListButton);
            this.Controls.Add(this.BuildList);
            this.Controls.Add(this.BuildTypeComboBox);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.ServerTypeComboBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.RemoveSelectedItemButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "RoBuilder";
            this.Text = "RoBuilder";
            this.Load += new System.EventHandler(this.ZoneBuilder_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button RemoveSelectedItemButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ServerTypeComboBox;
        private System.Windows.Forms.ComboBox BuildTypeComboBox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ListBox BuildList;
        private System.Windows.Forms.Button AddToBuildListButton;
        private System.Windows.Forms.Button PriorityUpButton;
        private System.Windows.Forms.Button PriorityDownButton;
    }
}

