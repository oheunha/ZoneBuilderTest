﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoBuilder
{
    class ControlHelper
    {
        delegate void EnableTextBoxCallback(Form form, TextBox textbox, bool enable);
        delegate void EnableButtonCallback(Form form, Button textbox, bool enable);
        delegate void EnableComboBoxCallback(Form form, ComboBox comboBox, bool enable);
        delegate void EnableListBoxCallback(Form form, ListBox listbox, bool enable);
        delegate void SetLabelCallback(Form form, Label label, string text);
        delegate void SetTextBoxCallback(Form form, TextBox textbox, string text);

        static public void EnableTextBox(Form form, TextBox textBox, bool enable)
        {
            if (textBox.InvokeRequired)
            {
                form.Invoke(new EnableTextBoxCallback(EnableTextBox), new object[] { form, textBox, enable });
            }
            else
            {
                textBox.Enabled = enable;
            }
        }

        static public void EnableButton(Form form, Button button, bool enable)
        {
            if (button.InvokeRequired)
            {
                form.Invoke(new EnableButtonCallback(EnableButton), new object[] { form, button, enable });
            }
            else
            {
                button.Enabled = enable;
            }
        }

        static public void EnableComboBox(Form form, ComboBox comboBox, bool enable)
        {
            if (comboBox.InvokeRequired)
            {
                form.Invoke(new EnableComboBoxCallback(EnableComboBox), new object[] { form, comboBox, enable });
            }
            else
            {
                comboBox.Enabled = enable;
            }
        }

        static public void EnableListBox(Form form, ListBox listBox, bool enable)
        {
            if (listBox.InvokeRequired)
            {
                form.Invoke(new EnableListBoxCallback(EnableListBox), new object[] { form, listBox, enable });
            }
            else
            {
                listBox.Enabled = enable;
            }
        }

        static public void SetLabel(Form form, Label label, string text)
        {
            if (label.InvokeRequired)
            {
                form.Invoke(new SetLabelCallback(SetLabel), new object[] { form, label, text });
            }
            else
            {
                label.Text = text;
            }
        }

        static public void SetTextBox(Form form, TextBox textBox, string text)
        {
            if (textBox.InvokeRequired)
            {
                form.Invoke(new SetTextBoxCallback(SetTextBox), new object[] { form, textBox, text });
            }
            else
            {
                textBox.Text = text;
            }
        }

        static public void UpdateListBoxItem(Form form, ListBox listbox, int index, string item)
        {
            if (listbox.InvokeRequired)
            {
                listbox.Invoke(new MethodInvoker(delegate ()
                {
                    if(index < listbox.Items.Count)
                    {
                        listbox.Items[index] = item;
                    }
                }));
            }
            else
            {
                if (index < listbox.Items.Count)
                {
                    listbox.Items[index] = item;
                }
            }
        }

        static public void AddListBoxItem(Form form, ListBox listbox, string item)
        {
            if (listbox.InvokeRequired)
            {
                listbox.Invoke(new MethodInvoker(delegate ()
                {
                    listbox.Items.Add(item);
                }));
            }
            else
            {
                listbox.Items.Add(item);
            }
        }

        static public void ClearListBoxItem(Form form, ListBox listbox)
        {
            if (listbox.InvokeRequired)
            {
                listbox.Invoke(new MethodInvoker(delegate ()
                {
                    listbox.Items.Clear();
                }));
            }
            else
            {
                listbox.Items.Clear();
            }
        }
    }
}
